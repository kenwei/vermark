# VerMark

## Overview

VerMark is  built upon [streamparse](https://github.com/Parsely/streamparse),
which is basically a python wrapper of [Apache Storm](https://github.com/apache/storm).

Currently, the verification process flow is implemented as - 
 
 1. Loading message from facts log
 2. Matching each loaded message on both systems (HBase and MySQL)
 3. Summarizing the result in Redis DB.

Sooner there shall be some other matching processes added in the flow to 
verify the notification of sub systems. 


## Setup 

### Vagrant

##### 1. Initiation

```
vagrant up
```

##### 2. SSH configuration

Add the box host name to */etc/hosts*

```
192.168.50.50 streamparse-box
```

And run below command to allow sparse to submit topologies to the box directly through ssh
 
```
vagrant ssh-config | sed -e 's/Host default/Host streamparse-box/' -e 's/HostName 127.0.0.1/HostName 192.168.50.50/' -e 's/Port 2222/Port 22/' -e 's/LogLevel FATAL/LogLevel INFO/' >> ~/.ssh/config
```

##### 3. Other Manual Stuffs

Install git dependency

```
sudo apt-get install git
```

Install mysql-config dependency

```
sudo apt-get install libmysqlclient-dev
```

Install ncurses lib dependency

```
sudo apt-get install libncurses5-dev
```

And copy both the fact logs and reMark source to desired location.


## Execution

##### 0. Environment Variables

Below listed the environment variables used by the project 

 1. *PROJECT_SRC* - the source of reMark
 2. *APP_ENV* - the env used by reMark
 3. *DATABASE_URL* - the URL to marketing DB
 4. *REDIS_URL* - the URL of the Redis DB to store the verification result
 5. *FACTS_URL* - the URL of fact logs

These variables can be overridden by spare command options which will be described later.

##### 1. At Local

The command to execute verification process generally looks as below 

```
sparse run -n <CONSUMER> [-o <OPTIONS>...] -t <EXECUTION_TIMEOUT> [-dv]
```

 1. *CONSUMER* - either **register_install** or **install_claim**
 2. *OPTIONS* - customized value of storm/spare options, or to override the environment variables mentioned above; details described later
 3. *EXECUTION_TIMEOUT* - seconds to force the execution to be terminated
 4. *-dv* - to enable debug logging

Please refer ```verify_install.sh``` or ```verify_claim.sh``` for examples.

##### 2. On Virtual Box (or other environments)

The command to submit the verification process to virtual box looks like

```
sparse submit -n <VERIFICATION_NAME> [-o <OPTIONS>...]
```

The definitions of the two options are the same as above. Please refer ```submit_install.sh``` or ```submit_claim.sh``` for examples.

##### 3. Command Options

 1. *remark.game* - the game ID to be used to override loaded fact logs   
 2. *remark.consumer* - the consumer name to be verified; default will use topology name for instead if not specified
 3. *remark.db.url* - database URL; will override environment variable ```DATABASE_URL``` if specified
 4. *remark.redis.url* - Redis URL; will override environment variable ```REDIS_URL``` if specified
 5. *remark.src.path* - reMark source path; will override environment variable ```PROJECT_PATH``` if specified
 6. *remark.env* - reMark environment setting name; will override environment variable ```APP_ENV``` if specified
 7. *remark.facts* - the path of the fact log file; will override environment variable ```FACTS_URL``` if specified

## Result Examination

The verification result will be stored in the given Redis DBs as following -
(if more than one cluster provided, the first one will be used to store summary and verification results, 
the last will be used to store temporary counts and data matching details)

##### 1. Matching Result

Stored as a ZSET with the naming ```remark/<STORM_ID>/matches:compare``` in Redis summary DB.
The key of the ZSET is the row number of the processed facts file, and the value indicates
whether this message is -

 1. Both matched - the value will be **2**
 2. Both not matched - the value will be **-2**
 3. Matched differently between the two system - the value will be **0** 

And, to count overall unmatched records, simply run below in python shell -

```
redis.StrictRedis().zcount('remark/<STORM_ID>/matches:compare', 0, 0)
```

Or to traverse the result by below

```
redis.StrictRedis().zrangebyscore('remark/<STORM_ID>/matches:compare', 0, 0, start=0, num=50)
```

##### 2. Matching Details
 
The matching of each message will be stored as a HASH with the naming 
```dptw/<CONSUMER>/details:<MESSAGE_ID>``` in Redis details DB.
The key and value of the HASH indicates the matching source and the corresponding result.

For example, a key-value pair 

```{ 'hbase' : (u'ody', u'693b2eba540a4d708a0d69e9c1bcc914', '00') }``` 

represents the message has a match in hbase by rowkey; on the other hand, a key-value pair 

```{ 'mysql' : 'no_match' }``` 

shows that there is not match found in mysql.


##### 3. Unique Key Counts

For the install records in HBase, there is also another metric can be used to 
determine whether any of the key likelihood exists.  

On Redis summary DB, if the Redis version is greater than 2.8, by using below 
command in python shell, the unique key count can be retrieved -
 
```
redis.StrictRedis().pfcount('remark/<STORM_ID>/ucount:hbase')
```

And on Redis details DB, below can fetch back the duplicated keys -

```
redis.StrictRedis().zrangebyscore('remark/<STORM_ID>/ukey:hbase', 2, 999)
```

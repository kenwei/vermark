import argparse
from collections import defaultdict
import redis

try:
    import ujson as json
except Exception:
    import json

parser = argparse.ArgumentParser(description='Verify install claim')
parser.add_argument('-r', '--redis_url', required=True, default='bistage-2-001.bi.qa.dal2.mz-inc.com:56379:2,bistage-2-001.bi.qa.dal2.mz-inc.com:56379:3',
                    help='The redis summary DB (and detail DB; separated by comma) URLs')
parser.add_argument('-t', '--topology_id', required=True, default='install_claim-2-1424856299',
                    help='The topology ID of storm claim verification process')
parser.add_argument('-l', '--facts_path', required=True, default='../facts/install_claim.revised.log',
                    help='The path of facts log used for the verification')
parser.add_argument('-d', '--show_details', required=False, default=True,
                    help='Whether to show comparison details')

def init_redis_client(redis_url):
    s_c = d_c = None
    for i, s in enumerate(redis_url.split(',')):
        host, port, db = s.split(':')
        client = redis.StrictRedis(host=host, port=int(port), db=int(db))
        if i == 0:
            s_c = client
        else:
            d_c = client

        if not d_c:
            d_c = s_c

    return s_c, d_c

def init_facts_loader(facts_file):
    with open(facts_file) as facts:
        for id, row in enumerate(facts):
            yield (id + 1, row)

def get_line(facts_loader, line):
    line_id = 0
    content = None
    while line_id < line:
        line_id, content = next(facts_loader)
    return content


def show_reason(source, reasons):
    print '\n * [%s] : ' % source
    for reason in reasons:
        lines = reasons[reason]
        print ' \t -- %s : %d \n\t\t%s' % (reason, len(lines), ', '.join(lines))


def main(topology_id, redis_url, facts_path, show_details):
    s_c, d_c = init_redis_client(redis_url)
    total_lines = s_c.zscore('remark/%s/processed' % topology_id, 'facts')
    hbase_matched_lines = s_c.zcount('remark/%s/matches:hbase' % topology_id, 1, 999)
    hbase_duplicated_lines = d_c.zcount('remark/%s/ukey:hbase' % topology_id , 2, 999)
    hbase_unique = d_c.zcount('remark/%s/ukey:hbase' % topology_id , 1, 999)

    mysql_matched_lines = s_c.zcount('remark/%s/matches:mysql' % topology_id, 1, 999)
    mysql_duplicated_lines = d_c.zcount('remark/%s/ukey:mysql' % topology_id , 2, 999)
    mysql_unique = d_c.zcount('remark/%s/ukey:mysql' % topology_id , 1, 999)

    diff_lines = s_c.zrangebyscore('remark/%s/matches:compare' % topology_id, 0, 0)
    mysql_not_matched = s_c.zrangebyscore('remark/%s/matches:mysql' % topology_id, 0, 0)
    hbase_not_matched = s_c.zrangebyscore('remark/%s/matches:hbase' % topology_id, 0, 0)
    h_n_m = [line for line in diff_lines if line in hbase_not_matched]
    m_n_h = [line for line in diff_lines if line in mysql_not_matched]

    print '===================== Comparison Result =====================\n'
    print ' * Claim Facts : %d \n' % total_lines

    print ' * [HBase] Matched            : %d '  % hbase_matched_lines
    print ' * [HBase] Duplicated Key     : %d '  % hbase_duplicated_lines
    print ' * [HBase] Unique Attribution : %d\n' % hbase_unique

    print ' * [MySQL] Matched            : %d '  % mysql_matched_lines
    print ' * [MySQL] Duplicated Key     : %d '  % mysql_duplicated_lines
    print ' * [MySQL] Unique Attribution : %d\n' % mysql_unique

    print ' * [Compare] HBase- MySQL+    : %d '   % len(h_n_m)
    print ' * [Compare] HBase+ MySQL-    : %d '   % len(m_n_h)

    if not show_details:
        return

    print '\n===================== Comparison Details =====================\n'
    facts = init_facts_loader(facts_path)
    diff_lines = sorted(diff_lines, key=int)
    hbase_reasons = defaultdict(list)
    mysql_reasons = defaultdict(list)
    for line in diff_lines:
        diff = d_c.hgetall('remark/%s/details:%s' % (topology_id, line))
        if line in h_n_m:
            hbase_reasons[diff['hbase']].append(line)
        else:
            mysql_reasons[diff['mysql']].append(line)
        print ' Line [%s] : %s \n%s\n' % (line, json.dumps(diff), get_line(facts, int(line)))

    print '\n===================== Comparison Summary ====================='

    show_reason('HBase', hbase_reasons)
    show_reason('MySQL', mysql_reasons)


if __name__ == '__main__':
    args = parser.parse_args()

    main(args.topology_id, args.redis_url, args.facts_path, args.show_details)

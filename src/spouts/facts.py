from common.storm_helper import StormHelper

try:
    import ujson as json
except ImportError:
    import json

from streamparse.spout import Spout


class FactsLogSpout(Spout):

    def _facts_loader(self, facts_file):
        with open(facts_file) as facts:
            for id, row in enumerate(facts):
                yield (id + 1, row)

    def initialize(self, storm_conf, context):
        file = StormHelper.get_fact(storm_conf)

        self.log("Initiating facts loader spout from {}"
                 .format(file))

        self.facts = self._facts_loader(file)
        self.game = StormHelper.get_game(storm_conf)
        self.redis = StormHelper.get_redis(storm_conf, 'facts')
        self.eor = False

    def next_tuple(self):
        if not self.eor:
            try:
                id, fact = next(self.facts)
            except StopIteration:
                self.eor = True
            else:
                if self.redis.shall_process(id):
                    msg = json.loads(fact[fact.index('{'):])
                    if 'game' in msg and self.game:
                        msg['game'] = self.game
                    self.emit([id, msg])

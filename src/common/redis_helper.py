import redis

class RedisHelper(object):

    def __init__(self,
                 project_name,
                 process_id,
                 category_name,
                 redis_url):
        if not redis_url:
            raise Exception("Incorrect redis settings")

        self.s_c = self.d_c = None
        for i, s in enumerate(redis_url.split(',')):
            host, port, db = s.split(':')
            client = redis.StrictRedis(host=host, port=int(port), db=int(db))
            if i == 0:
                self.s_c = client
            else:
                self.d_c = client

        if not self.d_c:
            self.d_c = self.s_c

        self.project = project_name
        self.pid = process_id
        self.cat = category_name

    def _get_key(self, key):
        return 'remark/%s/%s' % (self.pid, key)

    def count_processed(self):
        self.s_c.zincrby(self._get_key('processed'), self.cat, 1)

    def _shall_process(self, proc_key, msg_id):
        counted = self.d_c.hget(proc_key, msg_id)
        if not counted:
            self.count_processed()
            self.d_c.hset(proc_key, msg_id, 1)

        return counted <= 0

    def shall_process(self, msg_id):
        """
        Since currently we use facts log as spout source and storm is
        `guarantee once` streaming framework. There are very likely some chances
        that a tuple might be emitted multiple times. Therefore we will keep a
        counter on redis to know whether the message ID have been processed
        previously.
        """
        proc_key = self._get_key('ack:%s' % self.cat)
        return self._shall_process(proc_key, msg_id)

    def aggregate_matches(self, source, msg_id, matched):
        compare_key = self._get_key('%s:%s' % (self.cat, source))
        if self._shall_process(compare_key, msg_id):
            self.s_c.zincrby(self._get_key('matches:%s' % self.cat),
                             msg_id, 1 if matched else -1)

    def count_matched(self, msg_id, matched, data_key):
        # count by system type
        self.s_c.zincrby(self._get_key('matches:%s' % self.cat),
                         msg_id, 1 if matched else 0)
        # count by source_id
        if data_key:
            self.d_c.hset(self._get_key('%s:%s' % ('details', msg_id)),
                          self.cat, data_key)

    def count_key(self, key):
        try:
            count_key = self._get_key('ucount:%s' % self.cat)
            self.s_c.pfadd(count_key, key)
        except Exception:
            pass

        count_key = self._get_key('ukey:%s' % self.cat)
        self.d_c.zincrby(count_key, key, 1)
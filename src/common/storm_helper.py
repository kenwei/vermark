import sys
from common.mysql_helper import MySQLHelper
from common.redis_helper import RedisHelper
from common.settings import FACTS_URL, REDIS_URL, PROJECT_SRC, \
    DATABASE_URL, APP_ENV
import os

class StormHelper(object):

    PROJECT_NAME = 'remark'

    PROP_GAME = '%s.game' % PROJECT_NAME
    PROP_CONSUMER = '%s.consumer' % PROJECT_NAME
    PROP_FACTS = '%s.facts' % PROJECT_NAME
    PROP_REDIS = '%s.redis.url' % PROJECT_NAME
    PROP_DATABASE = '%s.db.url' % PROJECT_NAME
    PROP_SRC = '%s.src.path' % PROJECT_NAME
    PROP_ENV = '%s.env' % PROJECT_NAME

    @classmethod
    def get_consumer(cls, storm_conf):
        return storm_conf.get(cls.PROP_CONSUMER, storm_conf['topology.name'])

    @classmethod
    def get_fact(cls, storm_conf, consumer=None):
        consumer = consumer if consumer else cls.get_consumer(storm_conf)
        return storm_conf.get(cls.PROP_FACTS,
                              os.path.join(FACTS_URL,
                                           '{}.log'.format(consumer)))

    @classmethod
    def get_game(cls, storm_conf):
        return storm_conf.get(cls.PROP_GAME, None)

    @classmethod
    def get_redis(cls, storm_conf, proc_name, consumer=None):
        return RedisHelper(cls.PROJECT_NAME,
                           storm_conf.get('storm.id', cls.PROJECT_NAME),
                           proc_name,
                           storm_conf.get(cls.PROP_REDIS, REDIS_URL))

    @classmethod
    def get_mysql(cls, storm_conf):
        return MySQLHelper.get_instance(storm_conf.get(cls.PROP_DATABASE,
                                                       DATABASE_URL))

    @classmethod
    def setup_project(cls, storm_conf):
        src_path = storm_conf.get(cls.PROP_SRC, PROJECT_SRC)
        app_env = storm_conf.get(cls.PROP_ENV, APP_ENV)
        sys.path.append(src_path)
        os.environ['APP_ENV'] = app_env
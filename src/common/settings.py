import os

DATABASE_URL = os.getenv('DATABASE_URL', 'mysql://python:beth59_claim@mysql:3306/ody_marketing_localdev')

REDIS_URL = os.getenv('REDIS_URL', 'mysql:6379:2,mysql:6379:3')

PROJECT_SRC = os.getenv('PROJECT_SRC', '/vagrant/deps/dptw/src')

APP_ENV = os.getenv('APP_ENV', 'localdev')

FACTS_URL = os.getenv('FACTS_URL', '/vagrant/facts')

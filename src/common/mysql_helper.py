from common.settings import DATABASE_URL
from sqlalchemy import create_engine

class MySQLHelper(object):

    INSTANCES = dict({})

    @classmethod
    def get_instance(cls, db_url=DATABASE_URL):
        instance = cls.INSTANCES.get(db_url)
        if not instance:
            instance = MySQLHelper(db_url)
            cls.INSTANCES[db_url] = instance
        return instance

    def __init__(self, db_url=DATABASE_URL):
        self.engine = create_engine(db_url, pool_size=10)

    def _get_connect(self):
        return self.engine.connect()

    def execute_query(self, sql):
        return self._get_connect().execute(sql)

    def scalar(self, sql, **args):
        return self._get_connect().scalar(sql, **args)

    def fetchone(self, sql, **args):
        return self._get_connect().execute(sql, **args).fetchone()

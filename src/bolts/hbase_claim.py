from bolts import HBaseMatchBolt

class HBaseClaimMatchBolt(HBaseMatchBolt):

    def _match(self, msg):
        from models import ClaimRec, AttrRec
        from util.exceptions import ValidationError

        matched = False
        if 'partner' in msg and msg.get('partner'):
            try:
                claim = ClaimRec.from_msg(msg)
            except ValidationError as e:
                reason = "{}".format(e)
            else:
                result = claim.get_bound_install()
                if result:
                    rowkey = result['install'].rowkey()[0:3]
                    attr = AttrRec.get(rowkey)
                    if not attr:
                        reason = 'no_attr'
                    else:
                        matched = True
                        reason = rowkey
                        self.redis.count_key(rowkey)
                else:
                    reason = 'no_inst'
        else:
            reason = 'bad_partner'

        return matched, reason


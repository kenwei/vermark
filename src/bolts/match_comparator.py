from common.storm_helper import StormHelper
from streamparse.bolt import Bolt


class MatchComparatorBolt(Bolt):

    def initialize(self, storm_conf, context):
        self.redis = StormHelper.get_redis(storm_conf, 'compare')

    def process(self, tup):
        token, source, matched = tup.values
        self.redis.aggregate_matches(source, token, matched)

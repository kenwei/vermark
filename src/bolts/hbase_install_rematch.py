from common.storm_helper import StormHelper
from streamparse.bolt import Bolt


from models import InstallRec

class HBaseInstallAttrMatchBolt(Bolt):

    def initialize(self, storm_conf, context):
        StormHelper.setup_project(storm_conf)
        self.redis = StormHelper.get_redis(storm_conf, 'hbase')

    def _find_rec_in_hbase(self, game, id, key, value):
        try:
            return value and str(value).rstrip() and \
                   len(InstallRec.find_by('game_%s' % key,
                                          game, value)) > 0
        except Exception as e:
            self.log('==> find ID[{}] by ATTR[{}] but exception occurred, '
                     '{}'.format(id, key, e), 'debug')
            return False

    def process(self, tup):
        token, game, name, val = tup.values
        self.redis.count_matched(token,
                                 self._find_rec_in_hbase(token, game,
                                                         name, val))

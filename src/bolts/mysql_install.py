from bolts import MySQLMatchBolt

class MySQLInstallMatchBolt(MySQLMatchBolt):

    SQL = ('SELECT device_id '
           'FROM app_install '
           'WHERE {}')

    MSG_KEYS = ['idfa', 'imei', 'android_id',
                'adid', 'mac_address', 'token']

    def _rename_key(self, key):
        return '{}'.format({'adid': 'gaid',
                            'mac_address': 'mac_address_delim'}.get(key, key))

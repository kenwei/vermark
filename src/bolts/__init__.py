from common.storm_helper import StormHelper
from streamparse.bolt import Bolt

class BaseBolt(Bolt):

    TYPE = 'base'

    def initialize(self, storm_conf, context):
        StormHelper.setup_project(storm_conf)
        self.redis = StormHelper.get_redis(storm_conf, self.TYPE)

    def _match(self, msg):
        pass

    def process(self, tup):
        token, msg = tup.values

        if self.redis.shall_process(token):
            try:
                matched, key = self._match(msg)
            except Exception as e:
                self.log('Fail to process message [{}] on {}: {}'
                         .format(token, self.TYPE, e), 'warn')
                self.fail(tup.id)
            else:
                self.redis.count_matched(token, matched, key)
                self.emit([token, self.TYPE, matched])
        else:
            self.log('....Seems {} duplicated'.format(token))


class MySQLMatchBolt(BaseBolt):

    TYPE = 'mysql'

    SQL = ""

    CLAUSE = '{} = %({})s'

    MSG_KEYS = []

    def initialize(self, storm_conf, context):
        super(MySQLMatchBolt, self).initialize(storm_conf, context)
        self.sql = StormHelper.get_mysql(storm_conf)

    def _rename_key(self, key):
        return key

    def _normalize_val(self, key, val):
        return val

    def _execute_query(self, query, args):
        mysql_rec = self.sql.scalar(query, **args)
        return mysql_rec

    def _resolve(self, found):
        matched = found > 0
        rowkey = str(found) if matched else 'no_match'
        if matched:
            self.redis.count_key(rowkey)
        return matched, rowkey

    def _match(self, msg):
        found = None

        clauses = []
        arguments = {}
        for msg_key in self.MSG_KEYS:
            if msg_key in msg and msg[msg_key]:
                argument = self._normalize_val(msg_key, msg[msg_key])
                if argument:
                    clauses.append(self.CLAUSE.format(
                        self._rename_key(msg_key), msg_key))
                    arguments[msg_key] = argument

        if len(clauses):
            query = self.SQL.format(' OR '.join(clauses))
            found = self._execute_query(query, arguments)

        return self._resolve(found)


class HBaseMatchBolt(BaseBolt):

    TYPE = 'hbase'

    def initialize(self, storm_conf, context):
        super(HBaseMatchBolt, self).initialize(storm_conf, context)

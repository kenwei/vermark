from bolts import MySQLMatchBolt


class MySQLClaimMatchBolt(MySQLMatchBolt):

    SQL = """SELECT
               a.id,
               a.matched_by,
               b.idfa,
               b.android_id,
               b.imei,
               b.mac_address,
               b.gaid,
               b.ip,
               b.udid
             FROM
               partner_install_claims a
             LEFT JOIN app_install b on b.id = a.install_id
             WHERE
               ({}) and
               b.id is not null
    """

    MSG_KEYS = ['idfa', 'android_id', 'imei',
                'mac_address', 'gaid', 'ip', 'udid']

    def _rename_key(self, key):
        return 'b.{}'.format({'mac_address': 'mac_address_delim'}.get(key, key))

    def _execute_query(self, query, args):
        mysql_rec = self.sql.fetchone(query, **args)
        return mysql_rec

    def _resolve(self, found):
        if not found:
            return False, 'no_match'

        matched_by = found[1]
        rowkey = '%s:%s' % (matched_by, found[self.MSG_KEYS.index(matched_by) + 2])
        self.redis.count_key(rowkey)
        return True, rowkey

    def _normalize_val(self, key, val):
        value = val
        if val and key in ['gaid', 'mac_address']:
            try:
                from util.normalizer import normalize
                nkeys = {'gaid':'adid'}
                value = val if normalize(nkeys.get(key, key), val) else None
            except Exception as e:
                self.log('Invalid value, {}'.format(e), 'debug')
                value = None

        return value

from bolts import HBaseMatchBolt

class HBaseInstallMatchBolt(HBaseMatchBolt):

    def _match(self, msg):
        from models import InstallRec
        install = InstallRec.from_msg(msg)
        matched = install is not None
        rowkey = install.rowkey()
        if matched:
            self.redis.count_key(rowkey[0:3])
        return matched, rowkey if matched else 'no_match'

(ns install_claim
  (:use [backtype.storm.clojure]
        [streamparse.specs])
  (:gen-class))

(defn install_claim [options]
   [
    {"facts-log-spout" (
        python-spout-spec
            options
            "spouts.facts.FactsLogSpout"
            ["token" "msg"]
            :p 1)}

    {"hbase-claim-match-bolt" (
        python-bolt-spec
            options
            {"facts-log-spout" :shuffle}
            "bolts.hbase_claim.HBaseClaimMatchBolt"
            ["token" "source" "matched"]
            :p 3)

     "mysql-claim-match-bolt" (
        python-bolt-spec
            options
            {"facts-log-spout" :shuffle}
            "bolts.mysql_claim.MySQLClaimMatchBolt"
            ["token" "source" "matched"]
            :p 8)

      "match-compare-bolt" (
        python-bolt-spec
            options
            {"hbase-claim-match-bolt" ["token"]
             "mysql-claim-match-bolt" ["token"]}
            "bolts.match_comparator.MatchComparatorBolt"
            []
            :p 2)
    }
  ]
)

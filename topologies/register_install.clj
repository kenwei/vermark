(ns register_install
  (:use [backtype.storm.clojure]
        [streamparse.specs])
  (:gen-class))


(defn register_install [options]
   [
    {"facts-log-spout" (
        python-spout-spec
            options
            "spouts.facts.FactsLogSpout"
            ["token" "msg"]
            :p 1)}

    {"hbase-install-match-bolt" (
        python-bolt-spec
            options
            {"facts-log-spout" :shuffle}
            "bolts.hbase_install.HBaseInstallMatchBolt"
            ["token" "source" "matched"]
            :p 3)

     "mysql-install-match-bolt" (
        python-bolt-spec
            options
            {"facts-log-spout" :shuffle}
            "bolts.mysql_install.MySQLInstallMatchBolt"
            ["token" "source" "matched"]
            :p 8)

     "match-compare-bolt" (
        python-bolt-spec
            options
            {"hbase-install-match-bolt" ["token"]
             "mysql-install-match-bolt" ["token"]}
            "bolts.match_comparator.MatchComparatorBolt"
            []
            :p 2)}
  ]
)
